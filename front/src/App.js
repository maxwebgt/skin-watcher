import 'semantic-ui-css/semantic.min.css'
import Item from "./components/item";
import React, { useState } from 'react'
import Items from './items'
import {Container, Card, Image, Header, Segment, Menu } from "semantic-ui-react";
import orderBy from 'lodash/orderBy';



function App() {
    let [items, setItems] = useState([...Items]);
    const filterBy = filter => filter ? setItems([...Items].filter(e => e.asset_description.descriptions[0].value.includes(filter))) : setItems([...Items])

    const sortBy = (items, type) => {
        switch (type) {
            case 'price_high':
                setItems(orderBy(items, 'sell_price', 'desc'));
                break
            case 'name':
                setItems(orderBy(items, 'name', 'asc'));
                break
            default:
                return items;
        }
    };

  return (
      <Container>
          <Header as='h2' attached='top'>
              Предмет
          </Header>
          <Segment attached>
              <Card.Group itemsPerRow={10}>
                  <Card onClick={() => filterBy('Assault Rifle')}>
                      <Image src='/img/filter/rifle.ak.png' />
                  </Card>
                  <Card onClick={() => filterBy('M249')}>
                      <Image src='/img/filter/lmg.m249.png' />
                  </Card>
                  <Card onClick={() => filterBy('Bolt Action Rifle')}>
                      <Image src='/img/filter/rifle.bolt.png' />
                  </Card>
                  <Card onClick={() => filterBy('Rocket Launcher')}>
                      <Image src='/img/filter/rocket.launcher.png' />
                  </Card>
                  <Card onClick={() => filterBy('Garage Door')}>
                      <Image src='/img/filter/wall.frame.garagedoor.png' />
                  </Card>
                  <Card onClick={() => filterBy('')}>
                      {/*<Image src='/img/filter/wall.frame.garagedoor.png' />*/}
                      ВСЕ
                  </Card>
              </Card.Group>
          </Segment>
          <Segment attached>
          <Menu secondary >
              <Menu.Item
                  name='home'
                  // active={activeItem === 'home'}
                  // onClick={this.handleItemClick}
                  onClick={() => sortBy([...Items], 'name')}
              >По алфавиту</Menu.Item>
              <Menu.Item
                  name='messages'
                  // active={activeItem === 'messages'}
                  onClick={() => sortBy([...Items], 'price_high')}
              >По цене</Menu.Item>
          </Menu>
          </Segment>
          <Card.Group itemsPerRow={8}>
              {
                  items.map(e => <Item key={e.name} item={e}/>)
              }
          </Card.Group>
      </Container>
  );
}

export default App;
