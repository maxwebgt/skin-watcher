import React from 'react'
import { Card, Image } from 'semantic-ui-react'



const Item = (props) => {
    const {name, sell_price_text, asset_description} = props.item
    let icon = 'https://community.akamai.steamstatic.com/economy/image/' + asset_description.icon_url_large
    return (
        <Card>
            <Image src={icon} ui={false} />
            <Card.Content>
                <Card.Header>{name}</Card.Header>
                <Card.Meta>
                    <span className='date'>{sell_price_text}</span>
                </Card.Meta>
            </Card.Content>
        </Card>
    )
}

export default Item