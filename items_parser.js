const axios = require("axios");
const fs = require("fs");

const arr = []
const promiseArr = [];
let i = 0

while (i < 2000){
    promiseArr.push(
        axios.get('https://steamcommunity.com/market/search/render/?norender=1&appid=252490', {
            params: {
                start: i,
                count: 100
            }
        })
            .then(res => arr.push(...res.data.results))
            .catch(err => { console.error(err); return null; }) // раз уж мы не хотим ронять процесс при неудаче одного из запросов
    );
    i = i+100;
}

Promise.all(promiseArr)
    .then(results => {
        fs.writeFile("items.json", JSON.stringify(arr), function(error){
            if(error) throw error; // если возникла ошибка
            console.log("Асинхронная запись файла завершена");
        });

    })
    .then(filteredResults => "do something...")
.catch(err => 'you never know what will happen...');